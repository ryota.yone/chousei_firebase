import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyCUoNmY7OqnvsTMG3ifW_2eh3ZmlpcMuXI",
  authDomain: "chousei-firebase-bd2fa.firebaseapp.com",
  databaseURL: "https://chousei-firebase-bd2fa.firebaseio.com",
  projectId: "chousei-firebase-bd2fa",
  storageBucket: "",
  messagingSenderId: "66324351738",
  appId: "1:66324351738:web:526387a460fc5ae0"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
